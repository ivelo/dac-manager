from matplotlib import pyplot as plt
from cycler import cycler

def adjust_cycler(axs, cycle_type):
  if cycle_type == 'standard':
    dac_cycler = (cycler(alpha = [1, 0.5]) *
                  cycler(ls = ['-', '--', 'dotted']) *
                  cycler(color = ['red', 'darkred']))
    axs.set_prop_cycle(dac_cycler)
  elif cycle_type == 'compare_vdac_past':
    dac_cycler = (cycler(alpha = [1, 0.5]) *
                  cycler(ls = ['-', '--', 'dotted']) *
                  cycler(color = ['red', 'darkred']))
    axs.set_prop_cycle(dac_cycler)
  elif cycle_type == 'compare_idac_past':
    dac_cycler = (cycler(alpha = [1, 0.5]) *
                  cycler(ls = ['-', '--', 'dotted']) *
                  cycler(color = ['red', 'darkred']))
    axs.set_prop_cycle(dac_cycler)

def adjust_plot(axs, mod, dac, dac_def, ncol, plot_type):
  
  if plot_type == 'VDAC':
    axs.axhline(y = dac_def, lw = 2, c = 'k', ls = '--', alpha = 0.2,
                label = str(dac_def) + ' V default')
    axs.legend(fontsize = 8, ncol = 1, frameon = False)
    axs.set_xlabel('DAC value', fontsize = 12)
    axs.set_ylabel('Voltage [V]', fontsize = 12)
    axs.set_xlim([0,255])
    axs.set_ylim([0,1])
  elif plot_type == 'IDAC_I_DAC':
    axs.axvline(x = dac_def, lw = 2, c = 'k', ls = '--', alpha = 0.2,
                label = str(dac_def) + ' DAC default')
    axs.set_xlabel('DAC value', fontsize = 12)
    axs.set_ylabel('Current [A]', fontsize = 12)
    axs.set_xlim([0,255])
  elif plot_type == 'IDAC_I_V':
    axs.set_xlabel('Voltage [V]', fontsize = 12)
    axs.set_ylabel('Current [A]', fontsize = 12)
  elif plot_type == 'IDAC_V_DAC':
    axs.axvline(x = dac_def, lw = 2, c = 'k', ls = '--', alpha = 0.2,
                label = str(dac_def) + ' DAC default')
    axs.set_xlabel('DAC value', fontsize = 12)
    axs.set_ylabel('Voltage [V]', fontsize = 12)
    axs.set_xlim([0,255])

  axs.set_title('Module ' + str(mod).zfill(2) + ', DAC: ' + dac)
  axs.legend(fontsize = 8, ncol = ncol, frameon = False)
  axs.grid()

def adjust_hist(axs, title, xlabel):
  axs.set_xlabel(xlabel, fontsize = 12)
  axs.set_ylabel('Number of counts', fontsize = 12)
  axs.set_title(title)

def adjust_formatter(axs):
  formatter = ticker.ScalarFormatter(useMathText = True)
  formatter.set_scientific(True)
  formatter.set_powerlimits((-1,1))
  axs.xaxis.set_major_formatter(formatter)
