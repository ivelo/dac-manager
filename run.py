import os
import analyse

options = {
  ## basic options:
  "Modules" : range(1),#[25],
  "VDAC" : ['VFBK', 'VINCAS', 'VPREAMP_CAS', 'VCASDISC'],
  "VDAC_aim" : [.575, .660, .400, .850], # in mVs
  "VDAC_pseudodefault" : [143, 166, 97, 214],
  "IDAC" : ['IDISC', 'IKRUM', 'IPIXELDAC', 'IPREAMP'],
  "IDAC_aim" : [160, 32, 90, 164], # in DAC units
  "hardpath" : '/home/velo_user/dacscan_backup/',
  "input_data" : ['05052023/', '02052023/', '28042023/', '04042023/', '03042023/'],
  "output" : 'db_test1/',
  #"comment": 'no scan/asic objects, only abstract object',

  ## advanced options:
  "VFBK_gain_tolerance" : [.003, .0045],
  "VINCAS_gain_tolerance" : [.003, .0045],
  "VPREAMP_CAS_gain_tolerance" : [.003, .0045],
  "VCASDISC_gain_tolerance" : [.003, .0045],
  "IDISC_gain_tolerance" : [0.0006, 0.0014],
  "IKRUM_gain_tolerance" : [0.00002, 0.0005],
  "IPIXELDAC_gain_tolerance" : [0.0002, 0.0003],
  "IPREAMP_gain_tolerance" : [-0.0008, -0.0005],
  "IDISC_gain_tolerance_for_current" : [0.00065, 0.00085],
  "IPREAMP_gain_tolerance_for_current" : [0.00045,0.00065]
}

def main():
  build_fn = lambda tile_name, dac: tile_name + '.VP_DAC.DAC_' + dac + '.csv'
  if os.path.isdir(options["output"]) != True:
    os.mkdir(options["output"])
  dac = analyse.DacAnalyser(options = options, build_fn = build_fn)
  dac.setupdbentry()
  dac.vdac_analyse()
  dac.idac_analyse(find_from_current = True, use_all = True) #to check what use_all is used for
  dac.save()

if __name__ == '__main__':
  main()
