from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from typing import Callable
from scipy.optimize import curve_fit
from plot import adjust_cycler, adjust_plot, adjust_hist, adjust_formatter
import requests
import json
import os
from datetime import datetime

class DacAnalyser:
  def __init__(
    self,
    options: dict = None,
    build_fn: Callable[[str, str], str] = None,
  ):
    """
    Please do not modify this file!
    Options can be changed in run.py
    """
    self.options = options
    self.error_log = open(self.options["output"] +  'error_log.csv', 'w')
    self.df_dac = self.build_template_df()
    self.build_fn = build_fn
    self.api_host = 'http://127.0.0.1:8003'
    self.storage = '/calib/velo/.dac/'
    self.t = datetime.now()

  def _post(self, apath, data):
    session = requests.Session()
    session.trust_env = False
    content = session.post(
      "{}{}".format(self.api_host, '/api/' + apath),
      data = json.dumps(data),
      verify = False,
    )
    content.raise_for_status()
    return content.json()

  def setupdbentry(self):
    data = {'date': self.t.strftime('%H:%M:%S %d-%m-%Y'),
            'name': self.options['output'].replace('/',''),
            'uploaded_by': os.getlogin(),
            'uploaded_from': os.uname().nodename, 
            'comment': 'no scan objects, only abstract object'}
    self._post('dac', data = data)
    return None

  # a linear fit to the step function, the fit is evaluated at the edge of each step.
  # (used for current vs dac fit)
  def linear_edgefit(self, x, y):
    if len(x) != len(y):
      print("Warning: x and y does not have the same size in linear_edgefit, returning (np.nan, np.nan)")
      return (np.nan, np.nan)
    x_fit, y_fit = [], []
    for i in range(len(x) - 1):
      if y.iloc[i+1] != y.iloc[i]:
        x_fit.append( (x.iloc[i] + x.iloc[i+1])/2 )
        y_fit.append( (y.iloc[i] + y.iloc[i+1])/2 )
    gain, offset = np.polyfit(x_fit, y_fit, 1)
    return gain, offset


  def build_template_df(self):
    df_dac = pd.DataFrame(np.nan, index = range(624),
                          columns = ["Module", "ASIC"] + [dac for dac in
                          (self.options["VDAC"] + self.options["IDAC"])])
    
    df_dac["Module"] = ["Module" + str(int(i/12)).zfill(2) for i in range(624)]
    df_dac["ASIC"] = ["VP" + str(int((i%12)/3)) + '-' + str((i%12)%3) for i in range(624)]
    return df_dac

  def search_and_open(self, path_list):
    try:
      df = pd.read_csv(path_list[0], header = None, delimiter = ' ')
    except:
      if len(path_list[1:]) == 0:
        return 0, pd.DataFrame()
      else:
        return self.search_and_open(path_list[1:])
    return 1, df

  def vdac_analyse(self):
    for vdac, aim, pseudodefault in zip(self.options["VDAC"], self.options["VDAC_aim"], self.options["VDAC_pseudodefault"]):
      bound = self.options[vdac + "_gain_tolerance"]
      dac_fail = []
      print('Analysing voltage DAC: ' + str(vdac))
      for mod in self.options["Modules"]:
        error_code, if_plot, error_VPs  = 0, 0, ''

        fig, axs = plt.subplots(figsize = [6,5])
        adjust_cycler(axs, 'standard')

        for vp_id in range(12):
          chip, tile  = int(np.floor(vp_id /3)), int(np.floor(vp_id %3))

          #reading the datafiles
          tile_name = 'Module' + str(mod).zfill(2) + '_VP' + str(chip) + '-' + str(tile)
          path_list = []
          for scan in self.options["input_data"]:
            path_list.append(self.options["hardpath"] + scan + self.build_fn(tile_name, vdac))
          if_open, df = self.search_and_open(path_list)
          if if_open == 0:
            error_code = 1
            error_VPs += 'VP'+ str(chip) + '-' + str(tile) + ' '
          if if_open == 1:
            try:
              find_dac = int(df.iloc[(df.iloc[:,4] - aim).abs().argsort()[:1]].iloc[0,3])
              # the averaging below was added after equalisation studies.
              # ... in short: if we set following DACs too high, the equalisation gets worse because we cannot do the same with VTHR
              #print(f'... {vdac} before correction: {find_dac}')
              find_dac = int((find_dac + pseudodefault)/2)
              #print(f'... ... after correction towards {pseudodefault} is {find_dac}')
              axs.plot(df.iloc[:,3], df.iloc[:,4], label = tile_name + ' DAC: ' + str(find_dac))
              if_plot = 1
              gain, offset = np.polyfit(df.iloc[:,3], df.iloc[:,4], 1)

              if (gain > bound[0] and gain < bound[1]):
                self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, vdac] = int(find_dac)
              else:
                error_code = 1
                dac_fail.append(int(find_dac))
                error_VPs += 'VP' + str(chip) + '-' + str(tile) + ' '
              #self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, 
              #  vdac + "_gain"] = round(gain, 6)
            except:
              error_code = 1
              error_VPs += 'VP' + str(chip) + '-' + str(tile) + ' '

        if if_plot == 1 and error_code == 0:
          adjust_plot(axs, mod, vdac, aim, ncol = 1, plot_type = 'VDAC')
          print('Module ' + str(mod).zfill(2) + ' ' + vdac + ' : successful')
          plot_name = 'Mod' + str(mod).zfill(2) + '_' + vdac + '.png'
          plt.savefig(self.options["output"] + plot_name)
        elif if_plot == 1 and error_code == 1:
          adjust_plot(axs, mod, vdac, aim, ncol = 1, plot_type = 'VDAC')
          print('Module ' + str(mod).zfill(2) + ' ' + vdac + 
                ' : DAC not calculated ' + error_VPs)
          self.error_log.write('Module,' + str(mod).zfill(2) + ',VP,' + error_VPs + ',' +
                          vdac + ',DAC not calculated\n')    
          plot_name = 'Mod' + str(mod).zfill(2) + '_' + vdac + '.png'
          plt.savefig(self.options["output"] + plot_name)
        elif if_plot == 0:
          print('Module ' + str(mod).zfill(2) + ' ' + vdac + 
                ' : no data for that module')
          self.error_log.write('Module,' + str(mod).zfill(2) + ',VP,all,' + 
                          vdac + ',no data for that module\n')
        plt.close()

    
      #fig, ax = plt.subplots(figsize = [6,6])
      #n_of_bins = np.linspace(-0.5, 255.5, 129)

      #self.df_dac[(self.df_dac[vdac + "_gain"] < bound[1]) &
      #  (self.df_dac[vdac + "_gain"] > bound[0])].hist(vdac, 
      #  bins = n_of_bins, alpha = 0.95, ax = ax, color = 'k', grid = True,
      #  label = 'Gain OK', #'Gain out of margin'],
      #  histtype = 'step', lw = 1.5, stacked = True )
      #ax.hist(dac_fail, bins = n_of_bins, alpha = 0.95, color = 'r',
      #  label = 'Gain out of margin', histtype = 'step', lw = 1.5, stacked = True)
      #ax.set_xlim([0,255])
      #adjust_hist(ax, vdac, xlabel = 'DAC')
      #ax.legend(fontsize = 11)

      #plot_name = 'DAC_distribution_' + str(vdac) 
      #plt.savefig(self.options["output"] + plot_name + '.png')
      #plt.close()

    print('\nNew VDACs calculated: ', self.options["VDAC"])
    print('(NaN values will not update the config)')
    tscan = datetime.fromtimestamp(os.path.getctime(path))
    data = {'date': tscan.strftime('%H:%M:%S %d-%m-%Y'),
            'type': vdac,
            'VDa': os.getlogin(),
            'VDb': os.uname().nodename, 
            'rec': find_dac}
    self._post('dac', data = data)
    return None

  def idac_analyse(self, find_from_current = False, use_all = True):
    for idac, aim in zip(self.options["IDAC"], self.options["IDAC_aim"]):
      bound = self.options[idac + "_gain_tolerance"]
      if find_from_current == True and idac in ["IDISC", "IPREAMP"]:
        bound = self.options[idac + "_gain_tolerance_for_current"]
      if find_from_current == True and idac in ["IKRUM"]:
        print("Cannot find " + idac + " when relying on current")
        print("Assuming " + str(aim) + "DAC for " + idac)
        for mod in self.options['Modules']:
          for vp_id in range(12):
            chip, tile  = int(np.floor(vp_id /3)), int(np.floor(vp_id %3))
            tile_name = 'Module' + str(mod).zfill(2) + '_VP' + str(chip) + '-' + str(tile)
            self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, idac] = aim
        continue
      if find_from_current == True and idac in ["IPIXELDAC"]:
        print("Proceeding to IPIXELDAC.")
        for mod in self.options['Modules']:
          for vp_id in range(12):
            chip, tile = int(np.floor(vp_id/3)), int(np.floor(vp_id %3))
            try:
                #pixdat = np.loadtxt('/calib/velo/equalisation/equalis_NewPanel/Summer23/Module' + str(mod).zfill(2) + '/quick/Module' + 
                #        str(mod).zfill(2) + '_VP' + str(chip) + '-' + str(tile) + '_ipixeldac.txt')
                #tile_name = 'Module' + str(mod).zfill(2) + '_VP' + str(chip) + '-' + str(tile)
                #print(pixdat, tile_name)
                #self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, idac] = pixdat
                pass
            except:
                pass
        continue
      dac_fail = []
      print('Analysing current DAC: ' + str(idac))
            
      for mod in self.options["Modules"]:
        error_code, if_plot, error_VPs  = 0, 0, ''
        hist_at_aim = []

        plt.figure(1)
        fig1, axs1 = plt.subplots(figsize = [6,5])
        plt.figure(2)
        fig2, axs2 = plt.subplots(figsize = [6,5])
        plt.figure(3)
        fig3, axs3 = plt.subplots(figsize = [6,5])

        adjust_cycler(axs1, 'standard')
        adjust_cycler(axs2, 'standard')
        adjust_cycler(axs3, 'standard')

        for vp_id in range(12):
          chip, tile  = int(np.floor(vp_id /3)), int(np.floor(vp_id %3))

          #reading the datafile 
          tile_name = 'Module' + str(mod).zfill(2) + '_VP' + str(chip) + '-' + str(tile)
          path_list = []
          for scan in self.options["input_data"]:
            path_list.append(self.options["hardpath"] + scan + self.build_fn(tile_name, idac))

          if_open, df = self.search_and_open(path_list)
          if if_open == 0:
            error_code = 1
            error_VPs += 'VP'+ str(chip) + '-' + str(tile) + ' '
          if if_open == 1:
            try:
              if find_from_current == False:
                gain, offset = np.polyfit(df.iloc[50:250,3], df.iloc[50:250,4], 1)
              elif find_from_current == True:
                gain, offset = self.linear_edgefit(df.iloc[10:250,3], df.iloc[10:250,6])

            except:
              gain, offset = np.nan, np.nan
            try:
              if find_from_current == False:
                if (gain > bound[0] and gain < bound[1]):
                  at_aim = df[df.iloc[:,3] == aim].iloc[0,4]
                  hist_at_aim.append(at_aim)
                else:
                  error_code = 1
                  error_VPs += 'VP' + str(chip) + '-' + str(tile) + ' '
              if find_from_current == True:
                if (gain > bound[0] and gain < bound[1]):
                  pass
                else:
                  error_code = 1
                  error_VPs += 'VP' + str(chip) + '-' + str(tile) + ' '
                  
              self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, idac + "_gain"] = round(gain, 6)  
            except:
              error_code = 1
              error_VPs += 'VP' + str(chip) + '-' + str(tile) + ' '

          #axs1.plot(np.linspace(0,255,256), gain*np.linspace(0,255,256) + offset)
          axs1.plot(df.iloc[:,3], df.iloc[:,6], label = tile_name)
          axs2.plot(df.iloc[:,4], df.iloc[:,6], label = tile_name)
          axs3.plot(df.iloc[:,3], df.iloc[:,4], label = tile_name)
          if_plot = 1

        if if_plot == 1:
          adjust_plot(axs1, mod, idac, aim, ncol = 1, plot_type = 'IDAC_I_DAC')
          adjust_plot(axs2, mod, idac, aim, ncol = 1, plot_type = 'IDAC_I_V')
          adjust_plot(axs3, mod, idac, aim, ncol = 1, plot_type = 'IDAC_V_DAC')
          plot_name = 'Mod' + str(mod).zfill(2) + '_' + idac 
          plt.figure(fig1.number)
          plt.savefig(self.options["output"] + plot_name + '_I_DAC.png')
          plt.figure(fig2.number)
          plt.savefig(self.options["output"] + plot_name + '_I_V.png')
          plt.figure(fig3.number)
          plt.savefig(self.options["output"] + plot_name + '_V_DAC.png')
          if error_code == 0:
            print('Module ' + str(mod).zfill(2) + ' DAC: ' + idac + ' : successful')
          else:
            print('Module ' + str(mod).zfill(2) + ' DAC: ' + idac + 
              ' : gain out of expected margin for ' + error_VPs)
            self.error_log.write('Module,' + str(mod).zfill(2) + ',VP,' + error_VPs + ',' +
              idac + ',gain out of expected margin\n')  

        elif if_plot == 0: 
          print('Module ' + str(mod).zfill(2) + ' DAC: ' + idac +
            ' : failed to read data')
          self.error_log.write('Module,' + str(mod).zfill(2) + ',VP,all,' + 
            idac + ',failed to read data\n')
        plt.close('all')
      
      if find_from_current == False:
        av_aim = np.average(hist_at_aim)
        print('The average voltage for ' + str(aim) + ' ' 
          + idac + ' is ' + str(av_aim) + ' V')
      elif find_from_current == True:
        av_aim = self.df_dac[idac + "_gain"].mean() * aim
        print('The average current for ' + str(aim) + ' '
          + idac + ' is ' + str(av_aim) + ' A')
    
      for mod in self.options["Modules"]:
        for vp_id in range(12):
          chip, tile  = int(np.floor(vp_id /3)), int(np.floor(vp_id %3))
          tile_name = 'Module' + str(mod).zfill(2) + '_VP' + str(chip) + '-' + str(tile)
          path_list = []
          for scan in self.options["input_data"]:
            path_list.append(self.options["hardpath"] + scan + self.build_fn(tile_name, idac))

          if_open, df = self.search_and_open(path_list)
          if if_open == 1:

            gain = self.df_dac[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name].iloc[0][idac + "_gain"]
            if find_from_current == False:
              if gain > bound[0] and gain < bound[1]:
                try:
                  find_dac = df.iloc[(df.iloc[:,4] - av_aim).abs().argsort()[:1]].iloc[0,3]
                  self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, idac] = int(find_dac)
                except:
                  pass
              else:
                try:
                  dac_fail.append(int(find_dac))
                except:
                  pass
            elif find_from_current == True:
              if (gain > bound[0] and gain < bound[1]):
                find_dac = int(round(av_aim/gain))
                self.df_dac.loc[(self.df_dac['Module'] + '_' + self.df_dac['ASIC']) == tile_name, idac] = int(find_dac)
              else:
                try:
                  dac_fail.append(int(find_dac))
                except:
                  pass

      fig, ax = plt.subplots(figsize = [6,6])
      n_of_bins = np.linspace(-0.5, 255.5, 129)
      
      self.df_dac[(self.df_dac[idac + "_gain"] < bound[1]) &
        (self.df_dac[idac + "_gain"] > bound[0])].hist(idac, 
        bins = n_of_bins, alpha = 0.95, ax = ax, color = 'k', grid = True,
        label = 'Gain OK',
        histtype = 'step', lw = 1.5, stacked = True )
      ax.hist(dac_fail, bins = n_of_bins, alpha = 0.95, color = 'r',
        label = 'Gain out of margin', histtype = 'step', lw = 1.5, stacked = True)
      ax.set_xlim([0,255])
      adjust_hist(ax, idac, xlabel = 'DAC')
      ax.legend(fontsize = 11)

      plot_name = 'DAC_distribution_' + str(idac) 
      plt.savefig(self.options["output"] + plot_name + '.png')
      plt.close()

    print('\nNew IDACs calculated: ', self.options["IDAC"])
    print('(NaN values will not update the config)')    
    print(self.df_dac)

  def save(self):
    print("Saving the recipe...")
    self.df_dac.to_csv(self.options["output"] + 'recipe.csv', columns = ["Module", "ASIC"] +
      self.options["VDAC"] + self.options["IDAC"])
    print("Saving the error log...")
    self.error_log.close()


